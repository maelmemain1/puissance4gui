## **Description**
- Puissance 4 GUI : interface graphique pour mon [Puissance 4](https://gitlab.com/maelmemain1/puissance4) codé en une vingtaine d'heures avec _java awt_ et _java swing_.

## **État**
- Version JcJ fonctionnelle
- Version JcIA (coup gagnant et pour gagner) fonctionnelle
- Version JcIA (coup contrant) fonctionnelle

## **Niveau IA**
- L'ia est actuellement capable de jouer les coups lui permettant de gagner ainsi que de contrer les coups de l'adversaire pour l'empêcher de gagner.

## **Lancement**
- Sur **Windows** : 
    - Installer Java si vous ne l'avez pas
    - Lancer le fichier _Windows_launch.bat_
- Sur **Mac** et **Linux** : 
    - Installer Java si vous ne l'avez pas
    - Aller dans le répertoire du jeu avec le terminal
    - Lancer le script _Launch_MAC.sh_ avec la commande : `"bash MAC_launch.sh"`

## **Usage**
- Pour jouer :
    - il suffit de rentrer le nom du premier joueur, puis de rentrer le nom du second joueur ou bien de jouer contre le bot
    - ensuite lorsque la partie à démarrée, il suffit de rentrer le numéro de la colonne dans laquelle vous souhaitez jouer et de cliquer sur _Jouer_ pour placer votre pion.

## **Versions**
- Version actuelle :
    - v3.3 - 26/01/2024 : Améliorations de l'IA + fix des problèmes/bugs liés aux algos de l'IA.
- Anciennes versions :
    - v3.2 - 25/01/2024 : Correction de bugs.
    - v3.1 - 24/01/2024 : Ajout d'une icône à l'application.
    - v3.0 - 24/01/2024 : Changements dynamiques du titre et du curseur, réduction du nombre de lignes du code.
    - v2.5 - 24/01/2024 : Amélioration importante des algorithmes de l'IA, elle est maintenant moins prévisible.
    - v2.4 - 24/01/2024 : Amélioration des capacités de l'IA.
    - v2.3 - 24/01/2024 : Amélioration des capacités de l'IA.
    - v2.2 - 23/01/2024 : Version joueur contre IA (coup gagant et pour gagner + contre coups) fonctionnelle.
    - v2.1 - 23/01/2024 : Version joueur contre IA (coup gagant et pour gagner) fonctionnelle, version contre coups possède des bugs.
    - v2.0 - 23/01/2024 : Version joueur contre IA (coup gagant et pour gagner) fonctionnelle.
    - v1.1.2 - 15/06/2023 :  Ajout d'une page dans l'interface permettant d'entrer le nom des deux joueurs.
    - v1.1.1 - 12/06/2023 : Ajout d'un bouton **'Quitter'** pour quitter le jeu facilement.
    - v1.1 - 11/06/2023 : Modification de l'interface : divers ajouts et modifications pour embellir l'interface.
    - v1.0 - 31/03/2023 : Premiere release du projet

## Problèmes connus
- L'interface est distordue sur un écran en 2,5k -> **Un patch est en cours de développement.**
- Le bouton et la zone de texte deviennent parfois invisible -> **Ils réaparaissent en passant la souris dessus.**
- L'ia joue parfois des coups étranges -> **une amélioration de l'ia est en développement.**
- L'application est extremement gourmande en performance pour les petits cpus (peu de coeurs et threads) :
    - ~85+% utilisé sur un I5 7300U (2c4t) W11
    - xx% utilisés sur un I5 6267U (2c4t) Sonoma (à tester)
    - ~15% utilisé sur un I5 13500H (12c16t) W11 

## Infos
- 1854 lignes de code
- 1 classe

## **Crédits**
- Marloxdev - 11 Juin 2023
