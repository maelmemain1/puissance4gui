
/**
 * Jeu du puissance 4 avec interface 
 * @author marloxDev
 * @version 2.5
 * @since Dernière version : 23 janvier 2024
 * @see Puissance4
 */

import java.util.ArrayList;
import java.awt.*;
import java.awt.event.*;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.*;

public class Puissance4GUI {

    private static String[] jeu = new String[42];
    private static ArrayList<Label> labtab = new ArrayList<Label>(42);
    private static ArrayList<Label> bordsTab = new ArrayList<Label>(48);
    private static String j1, j2, jAct;
    private static int nbj;
    private static int caseJ;
    private static Frame fenetre = new Frame("Puissance4 made by marloxDev");
    private static JTextField saisie = new JTextField(5);
    private static JButton bouton = new JButton();
    private static JButton exit = new JButton();
    private static JButton bot = new JButton();

    private static int troisPBons = 0;
    private static int troisPBonsC = 0;
    private static String titreP = fenetre.getTitle();

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            }
        });

        setIcon("https://gitlab.com/maelmemain1/puissance4gui/-/raw/main/r/icon.jpg");
        affichagefx();
        fenetre.setLocationRelativeTo(null);
        fenetre.setCursor(new Cursor(3));
        intro();
        fenetre.setCursor(new Cursor(0));
        nomsJoueurs();
        changeOnFocus();
        jouer();
    }

    /**
     * Méthode qui permet de mettre une icône à l'application.
     */
    public static void setIcon(String url) {
        try {
            Image img = Toolkit.getDefaultToolkit()
                    .getImage(new URL(url));
            fenetre.setIconImage(img);

        } catch (MalformedURLException e1) {
            System.out.println("\033[1;31mErreur, URL Mal formée -> \033[0m" + e1);
        }
    }

    /**
     * Méthode qui permet de changer l'icône lorsque l'app n'est plus focus par
     * l'utilisateur et lorsqu'il retourne dessus.
     */
    public static void changeOnFocus() {
        fenetre.addWindowFocusListener(new WindowFocusListener() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                fenetre.setTitle(titreP);
                setIcon("https://gitlab.com/maelmemain1/puissance4gui/-/raw/main/r/icon.jpg");
            }

            @Override
            public void windowLostFocus(WindowEvent e) {
                fenetre.setTitle("Reviens jouer :'( ");
                setIcon("https://gitlab.com/maelmemain1/puissance4gui/-/raw/main/r/sad.png");

            }
        });
    }

    public static void nomsJoueurs() {
        fenetre.removeAll();
        JLabel esp = new JLabel("-----------------------------");
        JLabel esp2 = new JLabel("-----------------------------");
        JLabel esp3 = new JLabel(
                "---------------------------------------------------------------------------------------");
        JLabel txt = new JLabel("Nom du premier joueur :\n");

        exit.setText("Quitter");
        fenetre.add(esp);
        fenetre.add(exit);
        fenetre.add(esp2);
        fenetre.add(txt);
        fenetre.add(saisie);
        fenetre.add(esp3);
        bouton.setText("Ok");
        fenetre.add(bouton);
        fenetre.setVisible(true);

        j1 = null;
        while (j1 == null) {
            entree();
            entreeExit();
        }

        fenetre.removeAll();
        txt = new JLabel("Nom du second joueur :");
        bot.setText("Jouer contre le bot");

        fenetre.add(esp);
        fenetre.add(exit);
        fenetre.add(esp2);
        fenetre.add(txt);
        fenetre.add(saisie);
        bouton.setText("Ok");
        fenetre.add(bouton);
        fenetre.add(esp3);
        fenetre.add(bot);
        fenetre.setVisible(true);

        j2 = null;
        while (j2 == null) {
            entree();
            entreeBot();
            entreeExit();
        }
    }

    /**
     * Lancement de la partie de p4
     */
    public static void jouer() {

        remplirJeu();

        /*
         * TestJeuGagnantH();
         * TestJeuGagnantV();
         * TestJeuGagnantD();
         * System.out.println("--- Tests effectués ---");
         * attendre(800);
         */

        fenetre.setTitle("P4GUI by MarloxDev - " + j1 + " vs " + j2);
        titreP = fenetre.getTitle();

        affichagefx();

        jAct = "Joueur";

        nbj = (int) (Math.random() * 2);

        if (nbj == 1) {
            jAct = j1;
        } else {
            jAct = j2;
        }

        caseJ = -1;
        int coupG = -1;
        int coupC = -1;

        while (peutJouer() == true) {
            caseJ = -1;

            affichagefx();
            jAct = changeJ(jAct);

            if (jAct.equals("bot")) {

                troisPBons = 0;
                troisPBonsC = 0;

                while ((caseJ > 7 || caseJ < 1) || (lJouable(caseJ) == -1)) {

                    try {
                        coupG = botJoueCoup();
                        coupC = botContreCoup();

                        if ((coupC > 0 && troisPBonsC == 3) && (troisPBons < 3)) {
                            caseJ = coupC;
                        } else {
                            caseJ = coupG;
                        }

                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.err.println("\033[1;31mErreur caseJ invalide -> \033[0m" + e.getMessage());
                        caseJ = (int) (Math.random() * 6) + 1;
                    }
                }

                System.out.println();
                System.out.println("\033[1;33mLe bot a joue en : " + caseJ + ". \033[0m");
                System.out.println();
                veutJouer(jAct, lJouable(caseJ));

            } else {
                while ((caseJ > 7 || caseJ < 1) || (lJouable(caseJ) == -1)) {
                    try {
                        entreeExit();
                        entree();
                    } catch (Exception e) {
                        System.out.println("Erreur de saisie -> " + e.getMessage());
                    }

                }
                veutJouer(jAct, lJouable(caseJ));
            }
        }

        affichagefx();

        attendre(2000);

        fenetre.removeAll();

        if (jAct.equals("bot")) {
            setIcon("https://gitlab.com/maelmemain1/puissance4gui/-/raw/main/r/looser.jpg");
            fenetre.setTitle("P4GUI by MarloxDev - T'as perdu contre une ia gros looser L L L L L !");

        } else {
            setIcon("https://gitlab.com/maelmemain1/puissance4gui/-/raw/main/r/gg.jpg");
            fenetre.setTitle("P4GUI by MarloxDev - GG t'as win !");

        }

        Label gagnant = new Label("Le gagnant est : " + jAct);
        gagnant.setFont(new Font("Roboto", Font.BOLD, 15));
        Label cred = new Label("Puissance4GUI.java made by marloxDev");

        fenetre.add(gagnant);
        fenetre.setVisible(true);
        attendre(1500);
        fenetre.removeAll();

        fenetre.add(cred);
        fenetre.setVisible(true);
        attendre(1500);
        System.exit(0);
    }

    /**
     * Méthode qui permet de joure un coup pour contrer l'adversaire et l'empecher
     * de gagner
     * 
     * @return le coup à jouer
     */
    public static int botContreCoup() {
        int ret = -1;
        int i = 41;
        int k = (int) (Math.random() * 4);

        // lignes horizontales 3 bons
        while (ret < 0 && i > 3) {

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 1]))
                    && (jeu[i - 1].equals(jeu[i - 2]))
                    && ((jeu[i - 3]).equals("."))) {

                if (i < 35) {
                    if (jeu[i + 4].equals(".")) {
                        if (k % 3 == 0) {
                            troisPBonsC = 3;
                        } else {
                            troisPBonsC = 2;
                        }
                    } else {
                        troisPBonsC = 3;
                    }
                } else {
                    troisPBonsC = 3;
                }

                ret = numCaseToNumLigne(i - 3);
            }

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 2]))
                    && (jeu[i - 2].equals(jeu[i - 3]))
                    && ((jeu[i - 1]).equals("."))) {

                if (i < 35) {
                    if (jeu[i + 6].equals(".")) {
                        if (k % 3 == 0) {
                            troisPBonsC = 3;
                        } else {
                            troisPBonsC = 2;
                        }
                    } else {
                        troisPBonsC = 3;
                    }
                } else {
                    troisPBonsC = 3;
                }

                ret = numCaseToNumLigne(i - 1);
            }

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 1]))
                    && (jeu[i - 1].equals(jeu[i - 3]))
                    && ((jeu[i - 2]).equals("."))) {

                if (i < 35) {
                    if (jeu[i + 5].equals(".")) {
                        if (k % 3 == 0) {
                            troisPBonsC = 3;
                        } else {
                            troisPBonsC = 2;
                        }
                    } else {
                        troisPBonsC = 3;
                    }
                } else {
                    troisPBonsC = 3;
                }

                ret = numCaseToNumLigne(i - 2);
            }

            if (ret < 0 && (jeu[i - 3].equals("o")) && (jeu[i - 3].equals(jeu[i - 2]))
                    && (jeu[i - 2].equals(jeu[i - 1]))
                    && ((jeu[i]).equals("."))) {

                if (i < 35) {
                    if (jeu[i + 7].equals(".")) {
                        if (k % 3 == 0) {
                            troisPBonsC = 3;
                        } else {
                            troisPBonsC = 2;
                        }
                    } else {
                        troisPBonsC = 3;
                    }
                } else {
                    troisPBonsC = 3;
                }

                ret = numCaseToNumLigne(i);
            }

            if (i == 38 || i == 31 || i == 24 || i == 17 || i == 10 || i == 3) {
                i = i - 4;
            } else {
                i--;
            }
        }

        i = 41;

        // lignes verticales
        while (ret < 0 && i > 20) {

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 7]))
                    && (jeu[i - 7].equals(jeu[i - 14]))
                    && ((jeu[i - 21]).equals("."))) {
                troisPBonsC = 3;
                ret = numCaseToNumLigne(i - 21);
            }

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 7]))
                    && (jeu[i - 7].equals(jeu[i - 21]))
                    && ((jeu[i - 14]).equals("."))) {
                troisPBonsC = 3;
                ret = numCaseToNumLigne(i - 14);
            }

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 14]))
                    && (jeu[i - 14].equals(jeu[i - 21]))
                    && ((jeu[i - 7]).equals("."))) {
                troisPBonsC = 3;
                ret = numCaseToNumLigne(i - 7);
            }

            if (ret < 0 && (jeu[i - 7].equals("o")) && (jeu[i - 7].equals(jeu[i - 14]))
                    && (jeu[i - 14].equals(jeu[i - 21]))
                    && ((jeu[i]).equals("."))) {
                troisPBonsC = 3;
                ret = numCaseToNumLigne(i);
            }

            if (numCaseToNumLigne(i) == 4) {
                i = i - 4;
            } else {
                i--;
            }
        }

        i = 41;

        // lignes diagonales DtoG
        while (ret < 0 && i > 24) {

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 8]))
                    && (jeu[i - 8].equals(jeu[i - 16]))
                    && ((jeu[i - 24]).equals("."))) {

                if (jeu[i - 17].equals(".")) {
                    if (k % 3 == 0) {
                        troisPBonsC = 3;
                    } else {
                        troisPBonsC = 2;
                    }
                } else {
                    troisPBonsC = 3;
                }
                ret = numCaseToNumLigne(i - 24);
            }

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 8]))
                    && (jeu[i - 8].equals(jeu[i - 24]))
                    && ((jeu[i - 16]).equals("."))) {

                if (jeu[i - 9].equals(".")) {
                    if (k % 3 == 0) {
                        troisPBonsC = 3;
                    } else {
                        troisPBonsC = 2;
                    }
                } else {
                    troisPBonsC = 3;
                }
                ret = numCaseToNumLigne(i - 16);
            }

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 16]))
                    && (jeu[i - 16].equals(jeu[i - 24]))
                    && ((jeu[i - 8]).equals("."))) {

                if (jeu[i - 1].equals(".")) {
                    if (k % 3 == 0) {
                        troisPBonsC = 3;
                    } else {
                        troisPBonsC = 2;
                    }
                } else {
                    troisPBonsC = 3;
                }
                ret = numCaseToNumLigne(i - 8);
            }

            if (ret < 0 && (jeu[i - 8].equals("o")) && (jeu[i - 8].equals(jeu[i - 16]))
                    && (jeu[i - 16].equals(jeu[i - 24]))
                    && ((jeu[i]).equals("."))) {

                if (i < 35) {
                    if (jeu[i - 7].equals(".")) {
                        if (k % 3 == 0) {
                            troisPBonsC = 3;
                        } else {
                            troisPBonsC = 2;
                        }
                    } else {
                        troisPBonsC = 3;
                    }
                } else {
                    troisPBonsC = 3;
                }
                ret = numCaseToNumLigne(i);
            }

            if (numCaseToNumLigne(i) == 4) {
                i = i - 4;
            } else {
                i--;
            }
        }

        i = 38;

        // lignes diagonales GtoD
        while (ret < 0 && i > 24) {

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 6]))
                    && (jeu[i - 12].equals(jeu[i - 18]))
                    && ((jeu[i - 18]).equals("."))) {

                if (jeu[i - 11].equals(".")) {
                    if (k % 3 == 0) {
                        troisPBonsC = 3;
                    } else {
                        troisPBonsC = 2;
                    }
                } else {
                    troisPBonsC = 3;
                }
                ret = numCaseToNumLigne(i - 18);
            }

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 6]))
                    && (jeu[i - 6].equals(jeu[i - 18]))
                    && ((jeu[i - 12]).equals("."))) {

                if (jeu[i - 5].equals(".")) {
                    if (k % 3 == 0) {
                        troisPBonsC = 3;
                    } else {
                        troisPBonsC = 2;
                    }
                } else {
                    troisPBonsC = 3;
                }
                ret = numCaseToNumLigne(i - 12);
            }

            if (ret < 0 && (jeu[i].equals("o")) && (jeu[i].equals(jeu[i - 12]))
                    && (jeu[i - 12].equals(jeu[i - 18]))
                    && ((jeu[i - 6]).equals("."))) {

                if (jeu[i + 1].equals(".")) {
                    if (k % 3 == 0) {
                        troisPBonsC = 3;
                    } else {
                        troisPBonsC = 2;
                    }
                } else {
                    troisPBonsC = 3;
                }
                ret = numCaseToNumLigne(i - 6);
            }

            if (ret < 0 && (jeu[i - 6].equals("o")) && (jeu[i - 6].equals(jeu[i - 12]))
                    && (jeu[i - 12].equals(jeu[i - 18]))
                    && ((jeu[i]).equals("."))) {

                if (i < 35) {
                    if (jeu[i + 7].equals(".")) {
                        if (k % 3 == 0) {
                            troisPBonsC = 3;
                        } else {
                            troisPBonsC = 2;
                        }
                    } else {
                        troisPBonsC = 3;
                    }
                } else {
                    troisPBonsC = 3;
                }
                ret = numCaseToNumLigne(i);
            }

            if (numCaseToNumLigne(i) == 1) {
                i = i - 3;
            } else {
                i--;
            }
        }

        if (ret < 0 || colonnePleine(ret)) {
            System.out.println();
            System.out.println("\033[1;35mAucun contre coup a jouer : " + ret + ".\033[0m");
            System.out.println();
        } else {
            System.out.println();
            System.out.println("\033[1;32mCoup contrant trouve (" + troisPBonsC + "bons) : " + ret + ". \033[0m");
            System.out.println();
        }

        return ret;
    }

    /**
     * Méthode qui permet de faire jouer un coup gagnant à l'ia
     * 
     * @return le coup a jouer
     */
    public static int botJoueCoup() {
        int ret = -1;
        int i = 41;
        int k = (int) (Math.random() * 2);

        // lignes horizontales 3 bons
        while (ret < 0 && i > 3) {

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 1]))
                    && (jeu[i - 1].equals(jeu[i - 2]))
                    && ((jeu[i - 3]).equals("."))) {
                if (i < 35) {
                    if (jeu[i + 4].equals(".")) {
                        if (k % 2 == 0) {
                            troisPBons = 3;
                        } else {
                            troisPBons = 2;
                        }
                    } else {
                        troisPBons = 3;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i - 3);
            }

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 2]))
                    && (jeu[i - 2].equals(jeu[i - 3]))
                    && ((jeu[i - 1]).equals("."))) {
                if (i < 35) {
                    if (jeu[i + 6].equals(".")) {
                        if (k % 2 == 0) {
                            troisPBons = 3;
                        } else {
                            troisPBons = 2;
                        }
                    } else {
                        troisPBons = 3;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i - 1);
            }

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 1]))
                    && (jeu[i - 1].equals(jeu[i - 3]))
                    && ((jeu[i - 2]).equals("."))) {
                if (i < 35) {
                    if (jeu[i + 5].equals(".")) {
                        if (k % 2 == 0) {
                            troisPBons = 3;
                        } else {
                            troisPBons = 2;
                        }
                    } else {
                        troisPBons = 3;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i - 2);
            }

            if (ret < 0 && (jeu[i - 3].equals("x")) && (jeu[i - 3].equals(jeu[i - 2]))
                    && (jeu[i - 2].equals(jeu[i - 1]))
                    && ((jeu[i]).equals("."))) {

                if (i < 35) {
                    if (jeu[i + 7].equals(".")) {
                        if (k % 2 == 0) {
                            troisPBons = 3;
                        } else {
                            troisPBons = 2;
                        }
                    } else {
                        troisPBons = 3;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i);
            }

            if (numCaseToNumLigne(i) == 4) {
                i = i - 4;
            } else {
                i--;
            }
        }

        i = 41;

        // lignes verticales 3bons
        while (ret < 0 && i > 20)

        {

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 7]))
                    && (jeu[i - 7].equals(jeu[i - 14]))
                    && ((jeu[i - 21]).equals("."))) {
                troisPBons = 3;
                ret = numCaseToNumLigne(i - 21);
            }

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 7]))
                    && (jeu[i - 7].equals(jeu[i - 21]))
                    && ((jeu[i - 14]).equals("."))) {
                troisPBons = 3;
                ret = numCaseToNumLigne(i - 14);
            }

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 14]))
                    && (jeu[i - 14].equals(jeu[i - 21]))
                    && ((jeu[i - 7]).equals("."))) {
                troisPBons = 3;
                ret = numCaseToNumLigne(i - 7);
            }

            if (ret < 0 && (jeu[i - 7].equals("x")) && (jeu[i - 7].equals(jeu[i - 14]))
                    && (jeu[i - 14].equals(jeu[i - 21]))
                    && ((jeu[i]).equals("."))) {
                troisPBons = 3;
                ret = numCaseToNumLigne(i);
            }

            if (numCaseToNumLigne(i) == 4) {
                i = i - 4;
            } else {
                i--;
            }
        }

        i = 41;

        // lignes diagonales DtoG 3bons
        while (ret < 0 && i > 24) {

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 8]))
                    && (jeu[i - 8].equals(jeu[i - 16]))
                    && ((jeu[i - 24]).equals("."))) {

                if (jeu[i - 17].equals(".")) {
                    if (k % 2 == 0) {
                        troisPBons = 3;
                    } else {
                        troisPBons = 2;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i - 24);
            }

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 8]))
                    && (jeu[i - 8].equals(jeu[i - 24]))
                    && ((jeu[i - 16]).equals("."))) {

                if (jeu[i - 9].equals(".")) {
                    if (k % 2 == 0) {
                        troisPBons = 3;
                    } else {
                        troisPBons = 2;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i - 16);
            }

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 16]))
                    && (jeu[i - 16].equals(jeu[i - 24]))
                    && ((jeu[i - 8]).equals("."))) {

                if (jeu[i - 1].equals(".")) {
                    if (k % 2 == 0) {
                        troisPBons = 3;
                    } else {
                        troisPBons = 2;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i - 8);
            }

            if (ret < 0 && (jeu[i - 8].equals("x")) && (jeu[i - 8].equals(jeu[i - 16]))
                    && (jeu[i - 16].equals(jeu[i - 24]))
                    && ((jeu[i]).equals("."))) {

                if (i < 35) {
                    if (jeu[i - 7].equals(".")) {
                        if (k % 2 == 0) {
                            troisPBons = 3;
                        } else {
                            troisPBons = 2;
                        }
                    } else {
                        troisPBons = 3;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i);
            }

            if (numCaseToNumLigne(i) == 4) {
                i = i - 4;
            } else {
                i--;
            }
        }

        i = 38;

        // lignes diagonales GtoD 3bons
        while (ret < 0 && i > 24)

        {

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 6]))
                    && (jeu[i - 6].equals(jeu[i - 12]))
                    && ((jeu[i - 18]).equals("."))) {

                if (jeu[i - 11].equals(".")) {
                    if (k % 2 == 0) {
                        troisPBons = 3;
                    } else {
                        troisPBons = 2;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i - 18);
            }

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 6]))
                    && (jeu[i - 6].equals(jeu[i - 18]))
                    && ((jeu[i - 12]).equals("."))) {

                if (jeu[i - 5].equals(".")) {
                    if (k % 2 == 0) {
                        troisPBons = 3;
                    } else {
                        troisPBons = 2;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i - 12);
            }

            if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 12]))
                    && (jeu[i - 12].equals(jeu[i - 18]))
                    && ((jeu[i - 6]).equals("."))) {

                if (jeu[i + 1].equals(".")) {
                    if (k % 2 == 0) {
                        troisPBons = 3;
                    } else {
                        troisPBons = 2;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i - 6);
            }

            if (ret < 0 && (jeu[i - 6].equals("x")) && (jeu[i - 6].equals(jeu[i - 12]))
                    && (jeu[i - 12].equals(jeu[i - 18]))
                    && ((jeu[i]).equals("."))) {

                if (i < 35) {
                    if (jeu[i + 7].equals(".")) {
                        if (k % 2 == 0) {
                            troisPBons = 3;
                        } else {
                            troisPBons = 2;
                        }
                    } else {
                        troisPBons = 3;
                    }
                } else {
                    troisPBons = 3;
                }
                ret = numCaseToNumLigne(i);
            }

            if (numCaseToNumLigne(i) == 1) {
                i = i - 3;
            } else {
                i--;
            }
        }

        if (ret < 0 ||

                colonnePleine(ret)) {
            i = 41;
            // lignes horizontales 2 bons
            while (ret < 0 && i > 3) {
                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 1]))
                        && (jeu[i - 2].equals("."))
                        && ((jeu[i - 3]).equals("."))) {

                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 2);
                    } else {
                        ret = numCaseToNumLigne(i - 3);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 2]))
                        && (jeu[i - 3].equals("."))
                        && ((jeu[i - 1]).equals("."))) {

                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 1);
                    } else {
                        ret = numCaseToNumLigne(i - 3);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 3]))
                        && (jeu[i - 1].equals("."))
                        && ((jeu[i - 2]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 2);
                    } else {
                        ret = numCaseToNumLigne(i - 1);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i - 3].equals("x")) && (jeu[i - 3].equals(jeu[i - 2]))
                        && (jeu[i - 1].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 1);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i - 3].equals("x")) && (jeu[i - 3].equals(jeu[i - 1]))
                        && (jeu[i - 2].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 2);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i - 1].equals("x")) && (jeu[i - 1].equals(jeu[i - 2]))
                        && (jeu[i - 3].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i);
                    } else {
                        ret = numCaseToNumLigne(i - 3);

                    }
                    troisPBons = 2;

                }

                if (numCaseToNumLigne(i) == 4) {
                    i = i - 4;
                } else {
                    i--;
                }
            }

            i = 41;

            // lignes verticales 2 bons
            while (ret < 0 && i > 20) {
                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 7]))
                        && (jeu[i - 14].equals("."))
                        && ((jeu[i - 21]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 14);
                    } else {
                        ret = numCaseToNumLigne(i - 21);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 21]))
                        && (jeu[i - 7].equals("."))
                        && ((jeu[i - 14]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 7);
                    } else {
                        ret = numCaseToNumLigne(i - 14);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 14]))
                        && (jeu[i - 21].equals("."))
                        && ((jeu[i - 7]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 21);
                    } else {
                        ret = numCaseToNumLigne(i - 7);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i - 7].equals("x")) && (jeu[i - 7].equals(jeu[i - 14]))
                        && (jeu[i - 21].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 21);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i - 21].equals("x")) && (jeu[i - 21].equals(jeu[i - 14]))
                        && (jeu[i - 7].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 7);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;

                }
                if (ret < 0 && (jeu[i - 21].equals("x")) && (jeu[i - 21].equals(jeu[i - 7]))
                        && (jeu[i - 14].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 14);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;

                }

                i--;
            }

            i = 41;

            // lignes diagonales DtoG 2bons
            while (ret < 0 && i > 24) {

                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 8]))
                        && (jeu[i - 16].equals("."))
                        && ((jeu[i - 24]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 16);
                    } else {
                        ret = numCaseToNumLigne(i - 24);
                    }
                    troisPBons = 2;

                }

                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 16]))
                        && (jeu[i - 24].equals("."))
                        && ((jeu[i - 8]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 24);
                    } else {
                        ret = numCaseToNumLigne(i - 8);
                    }
                    troisPBons = 2;
                }

                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 24]))
                        && (jeu[i - 16].equals("."))
                        && ((jeu[i - 8]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 16);
                    } else {
                        ret = numCaseToNumLigne(i - 8);

                    }
                    troisPBons = 2;
                }

                if (ret < 0 && (jeu[i - 8].equals("x")) && (jeu[i - 8].equals(jeu[i - 16]))
                        && (jeu[i - 24].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 24);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;
                }

                if (ret < 0 && (jeu[i - 8].equals("x")) && (jeu[i - 8].equals(jeu[i - 24]))
                        && (jeu[i - 16].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 16);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;
                }

                if (ret < 0 && (jeu[i - 16].equals("x")) && (jeu[i - 16].equals(jeu[i - 24]))
                        && (jeu[i - 8].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 8);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;
                }

                if (numCaseToNumLigne(i) == 4) {
                    i = i - 4;
                } else {
                    i--;
                }
            }

            i = 38;

            // lignes diagonales GtoD 2bons
            while (ret < 0 && i > 24) {

                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 6]))
                        && (jeu[i - 12].equals("."))
                        && ((jeu[i - 18]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 12);
                    } else {
                        ret = numCaseToNumLigne(i - 18);

                    }
                    troisPBons = 2;
                }

                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 12]))
                        && (jeu[i - 18].equals("."))
                        && ((jeu[i - 12]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 18);
                    } else {
                        ret = numCaseToNumLigne(i - 12);

                    }
                    troisPBons = 2;
                }

                if (ret < 0 && (jeu[i].equals("x")) && (jeu[i].equals(jeu[i - 18]))
                        && (jeu[i - 12].equals("."))
                        && ((jeu[i - 6]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 12);
                    } else {
                        ret = numCaseToNumLigne(i - 6);

                    }
                    troisPBons = 2;
                }

                if (ret < 0 && (jeu[i - 6].equals("x")) && (jeu[i - 6].equals(jeu[i - 12]))
                        && (jeu[i - 18].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 18);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;
                }

                if (ret < 0 && (jeu[i - 6].equals("x")) && (jeu[i - 6].equals(jeu[i - 18]))
                        && (jeu[i - 12].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 12);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                    troisPBons = 2;
                }

                if (ret < 0 && (jeu[i - 18].equals("x")) && (jeu[i - 18].equals(jeu[i - 12]))
                        && (jeu[i - 6].equals("."))
                        && ((jeu[i]).equals("."))) {
                    if (k % 2 == 0) {
                        ret = numCaseToNumLigne(i - 6);
                    } else {
                        ret = numCaseToNumLigne(i);

                    }
                }

                if (numCaseToNumLigne(i) == 1) {
                    i = i - 3;
                } else {
                    i--;
                }
            }

            if (ret < 0 || colonnePleine(ret)) {
                ret = (int) (Math.random() * 6) + 1;
                System.out.println();
                System.out.println("\033[1;35mAucun coup gagnant, coup aleatoire joue : " + ret + ".\033[0m");
                System.out.println();
            }

        }

        if (troisPBons > 0) {
            System.out.println();
            System.out.println("\033[1;32mCoup gagnant trouve (" + troisPBons + "bons) : " + ret + ". \033[0m");
            System.out.println();
        }

        return ret;
    }

    /**
     * Méthode qui indique si une colonne est pleine ou non
     * 
     * @param colonne le numéro de la colonne
     * @return true si colonne pleine false sinon
     */
    public static boolean colonnePleine(int colonne) {
        boolean ret = true;
        if (jeu[colonne - 1].equals(".")) {
            ret = false;
        }

        return ret;
    }

    /**
     * Méthode qui prends en entré le numéro de la case du jeu et qui renvoi le
     * numéro de la colonne correspondante
     * 
     * @param numCase le numéro de la case
     * @return la colonne correspondante
     */
    public static int numCaseToNumLigne(int numCase) {
        return (numCase % 7) + 1;
    }

    /**
     * Méthode qui permet de placer la piece dans le plateau
     * 
     * @param jAct  la colonne dans laquelle le joueur veut jouer
     * @param caseJ le nom du joueur actuel
     */
    public static void veutJouer(String jAct, int caseJ) {
        if (jAct.equals(j1)) {
            jeu[caseJ] = "o";
        } else {
            jeu[caseJ] = "x";
        }
    }

    /**
     * Méthode qui indique si une colonne est jouable et qui place le pion dans la
     * bonne case en fonction en prenant en compte les autres pieces dans le plateau
     * et l'attraction gravitationnelle pour ne pas avoir un piece qui vole mdr.
     * 
     * @param caseJ le numéro de la colonne ou l'on veut jouer
     * @return le numéro de la case dans laquelle le pion sera placé
     */
    public static int lJouable(int caseJ) {
        int ret = -1;
        caseJ = caseJ - 1;
        if (caseJ != -1) {
            if (jeu[caseJ].equals(".")) {
                ret = caseJ;
                if (jeu[(caseJ + 7)].equals(".")) {
                    ret = (caseJ + 7);
                    if (jeu[(caseJ + 14)].equals(".")) {
                        ret = (caseJ + 14);
                        if (jeu[(caseJ + 21)].equals(".")) {
                            ret = (caseJ + 21);
                            if (jeu[(caseJ + 28)].equals(".")) {
                                ret = (caseJ + 28);
                                if (jeu[(caseJ + 35)].equals(".")) {
                                    ret = (caseJ + 35);
                                }
                            }
                        }
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Méthode booléenne qui indique s'il est encore possible de jouer en cherchant
     * s'il y'a des cases vides dans le jeu
     * 
     * @return true si il en reste
     */
    public static boolean peutJouer() {
        boolean ret = true;
        int i = 0;
        int sum = 0;
        while (i < jeu.length) {
            if (jeu[i].equals(".") == true) {
                sum++;
            }
            i++;
        }
        if (sum < 1) {
            ret = false;
        }

        if (jeuGagnant()) {
            ret = false;
        }

        return ret;
    }

    /**
     * Méthode qui cherche si la partie a été gagné en vérifiant toutes les
     * positions de victoires dans le jeu
     * 
     * @return true si la partie a été gagnée
     */
    public static boolean jeuGagnant() {
        boolean ret = false;
        int i = 0;
        int j = 0;
        int k = 0;
        // Horizontales
        while (j < 6) {
            k = 0;
            while (k < 4) {
                if ((jeu[i].equals("o")) || (jeu[i].equals("x"))) {
                    if ((jeu[i].equals(jeu[i + 1])) && (jeu[i + 1].equals(jeu[i + 2]))
                            && (jeu[i + 2].equals(jeu[i + 3]))) {
                        jeu[i] = "g";
                        jeu[i + 1] = "g";
                        jeu[i + 2] = "g";
                        jeu[i + 3] = "g";
                        ret = true;
                    }
                }
                i++;
                k++;
            }
            i = i + 3;
            j++;
        }

        // Verticales
        i = 0;
        j = 0;
        while (j < 7) {
            k = 0;
            while (k < 3) {
                if ((jeu[i].equals("o")) || (jeu[i].equals("x"))) {
                    if ((jeu[i].equals(jeu[i + 7])) && (jeu[i + 7].equals(jeu[i + 14]))
                            && (jeu[i + 14].equals(jeu[i + 21]))) {
                        jeu[i] = "g";
                        jeu[i + 7] = "g";
                        jeu[i + 14] = "g";
                        jeu[i + 21] = "g";
                        ret = true;
                    }
                }
                i = i + 7;
                k++;
            }
            j++;
            i = j;
        }

        // Diagonales
        i = 0;

        while (i < 40 && !ret) {

            if ((i > 2 && i < 7) || (i > 9 && i < 14) || (i > 16 && i < 21)) {
                if (diagGagnanteBG(i)) {
                    ret = true;
                }
            } else {
                if ((i >= 0 && i < 4) || (i >= 7 && i < 11) || (i >= 14 && i < 18)) {
                    if (diagGagnanteBD(i)) {
                        ret = true;
                    }
                }
            }

            if ((i > 20 && i < 25) || (i > 27 && i < 32) || (i > 34 && i < 39)) {
                if (diagGagnanteHD(i)) {
                    ret = true;
                }
            } else {
                if ((i > 23 && i < 28) || (i > 30 && i < 35) || (i > 37 && i <= 41)) {
                    if (diagGagnanteHG(i)) {
                        ret = true;
                    }
                }
            }
            i++;
        }
        return ret;
    }

    /**
     * Teste si une diagonale en partant de l'indice i est gagnante
     * 
     * @param i l'indice duquel tester la diagonale
     * @return true si la diagonale est gagnante
     */
    public static boolean diagGagnanteHG(int i) {
        boolean ret = false;
        if ((jeu[i].equals("o")) || (jeu[i].equals("x"))) {
            if ((jeu[i].equals(jeu[i - 8])) && (jeu[i - 8].equals(jeu[i - 16])) && (jeu[i - 16].equals(jeu[i - 24]))) {
                jeu[i] = "g";
                jeu[i - 8] = "g";
                jeu[i - 16] = "g";
                jeu[i - 24] = "g";
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Teste si une diagonale vers le haut droit en partant de l'indice i est
     * gagnante
     * 
     * @param i l'indice duquel tester la diagonale
     * @return true si la diagonale est gagnante
     */
    public static boolean diagGagnanteHD(int i) {
        boolean ret = false;
        if ((jeu[i].equals("o")) || (jeu[i].equals("x"))) {
            if ((jeu[i].equals(jeu[i - 6])) && (jeu[i - 6].equals(jeu[i - 12])) && (jeu[i - 12].equals(jeu[i - 18]))) {
                jeu[i] = "g";
                jeu[i - 6] = "g";
                jeu[i - 12] = "g";
                jeu[i - 18] = "g";
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Teste si une diagonale en partant de l'indice i est gagnante
     * 
     * @param i l'indice duquel tester la diagonale
     * @return true si la diagonale est gagnante
     */
    public static boolean diagGagnanteBD(int i) {
        boolean ret = false;
        if ((jeu[i].equals("o")) || (jeu[i].equals("x"))) {
            if ((jeu[i].equals(jeu[i + 8])) && (jeu[i + 8].equals(jeu[i + 16])) && (jeu[i + 16].equals(jeu[i + 24]))) {
                jeu[i] = "g";
                jeu[i + 8] = "g";
                jeu[i + 16] = "g";
                jeu[i + 24] = "g";
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Teste si une diagone en partant de l'indice i est gagnante
     * 
     * @param i l'indice duquel tester la diagonale
     * @return true si la diagonale est gagnante
     */
    public static boolean diagGagnanteBG(int i) {
        boolean ret = false;
        if ((jeu[i].equals("o")) || (jeu[i].equals("x"))) {
            if ((jeu[i].equals(jeu[i + 6])) && (jeu[i + 6].equals(jeu[i + 12])) && (jeu[i + 12].equals(jeu[i + 18]))) {
                jeu[i] = "g";
                jeu[i + 6] = "g";
                jeu[i + 12] = "g";
                jeu[i + 18] = "g";
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Permet de patienter pendant x temps en ms
     * 
     * @param temps le temps à patienter en ms
     */
    public static void attendre(long temps) {
        long t1 = System.currentTimeMillis();
        long t2 = 0;
        long diffT = 0;
        diffT = t2 - t1;
        while (diffT < temps) {
            t2 = System.currentTimeMillis();
            diffT = t2 - t1;
        }
    }

    /*
     * Affiche les println de début de jeu + les crédits...
     */
    public static void intro() {
        setIcon("https://gitlab.com/maelmemain1/puissance4gui/-/raw/main/r/icon.jpg");
        String version = "3.0";
        String vBot = "Coups gagnants + coups contrants";
        String etat = "Versions JcJ et JcIA operationnelles";

        fenetre.removeAll();
        Label intro1 = new Label("Lancement de Puissance4.java");
        Label intro2 = new Label("Version du jeu : V" + version + " Version Bot : " + vBot);
        Label intro3 = new Label("Developpe par Mael MEMAIN");
        Label intro4 = new Label("Debut : 31/03/2023 " + "   " + "Derniere version : 23/01/2024");
        Label intro5 = new Label("Etat : " + etat);
        fenetre.add(intro1);
        attendre(500);
        fenetre.setVisible(true);
        fenetre.add(intro2);
        attendre(500);
        fenetre.setVisible(true);
        fenetre.add(intro3);
        attendre(500);
        fenetre.setVisible(true);
        fenetre.add(intro4);
        attendre(500);
        fenetre.setVisible(true);
        fenetre.add(intro5);
        attendre(500);
        fenetre.setVisible(true);
        attendre(2000);
    }

    /**
     * Méthode qui permet de changer de joueur à chaque tour
     * 
     * @param jAct le joueur actuel
     */
    public static String changeJ(String jAct) {
        String ret;
        if (jAct == j2) {
            ret = j1;
        } else {
            ret = j2;
        }
        return ret;
    }

    /**
     * Méthode permettant de remplir le jeu avec les numéros de 0 à 41 pour les 42
     * cases
     */
    public static void remplirJeu() {
        int i = 0;

        while (i < jeu.length) {
            jeu[i] = ".";
            i++;
        }
    }

    /**
     * Affichage du jeu à l'aide d'une interface awt et swing
     */
    public static void affichagefx() {
        setIcon("https://gitlab.com/maelmemain1/puissance4gui/-/raw/main/r/icon.jpg");
        fenetre.setCursor(new Cursor(3));

        fenetre.removeAll();
        fenetre.setBackground(Color.LIGHT_GRAY);
        fenetre.setForeground(Color.black);
        fenetre.setSize(400, 580);
        fenetre.setAlwaysOnTop(true);
        fenetre.setResizable(false);

        bouton.setText("Jouer");
        exit.setText("Quitter");

        Label txt = new Label("Au tour de : ");

        fenetre.setLayout(new FlowLayout());

        // Création de l'affichage du jeu

        // Création des contours :
        Label chiffres = new Label("1          2          3          4          5         6          7");
        Label horizontalA = new Label(
                "----------------------------------------------------------------------------------------");
        Label horizontalB = new Label(
                "----------------------------------------------------------------------------------------");
        Label horizontalC = new Label(
                "----------------------------------------------------------------------------------------");
        Label horizontalD = new Label(
                "----------------------------------------------------------------------------------------");
        Label horizontalE = new Label(
                "----------------------------------------------------------------------------------------");
        Label horizontalF = new Label(
                "----------------------------------------------------------------------------------------");
        Label horizontalG = new Label(
                "----------------------------------------------------------------------------------------\n\n");

        chiffres.setForeground(Color.black);
        chiffres.setFont(new Font("Roboto", Font.BOLD, 15));

        horizontalA.setForeground(Color.black);
        horizontalA.setFont(new Font("Roboto", Font.PLAIN, 15));
        horizontalB.setForeground(Color.black);
        horizontalB.setFont(new Font("Roboto", Font.PLAIN, 15));
        horizontalC.setForeground(Color.black);
        horizontalC.setFont(new Font("Roboto", Font.PLAIN, 15));
        horizontalD.setForeground(Color.black);
        horizontalD.setFont(new Font("Roboto", Font.PLAIN, 15));
        horizontalE.setForeground(Color.black);
        horizontalE.setFont(new Font("Roboto", Font.PLAIN, 15));
        horizontalF.setForeground(Color.black);
        horizontalF.setFont(new Font("Roboto", Font.PLAIN, 15));
        horizontalG.setForeground(Color.black);
        horizontalG.setFont(new Font("Roboto", Font.PLAIN, 15));

        String ja = j1;
        if (jAct == j1) {
            ja = j2;
        }
        Label joueur = new Label(ja);
        if (jAct == j1) {
            joueur.setForeground(Color.yellow);
        } else {
            joueur.setForeground(Color.red);
        }

        // Création des cases

        fenetre.removeAll();
        labtab.clear();
        majDuJeu();

        int i = 0;
        while (i < 48) {
            bordsTab.add(new Label("|"));
            i++;
        }
        i = 0;

        // Ajout des cases et des contours : création de l'interface
        fenetre.add(exit);
        fenetre.add(chiffres);
        fenetre.add(horizontalA);
        rempliraffichagefx(0, 0);

        fenetre.add(horizontalB);
        rempliraffichagefx(8, 7);

        fenetre.add(horizontalC);
        rempliraffichagefx(16, 14);

        fenetre.add(horizontalD);
        rempliraffichagefx(24, 21);

        fenetre.add(horizontalE);
        rempliraffichagefx(32, 28);

        fenetre.add(horizontalF);
        rempliraffichagefx(40, 35);
        fenetre.add(horizontalG);

        if (j1 != null && joueur.getText().equals(j1) && titreP != null) {
            fenetre.setTitle(titreP + " (votre tour)");
        } else {
            fenetre.setTitle(titreP);
        }

        fenetre.add(txt);
        fenetre.add(joueur);
        fenetre.add(saisie);
        fenetre.add(bouton);

        bouton.setForeground(Color.blue);

        fenetre.setCursor(new Cursor(0));

        fenetre.setVisible(true);

        fenetre.setVisible(true);
    }

    /**
     * Méthode qui permet de mettre à jour l'affichage des labels du jeu
     */
    public static void majDuJeu() {
        int i = 0;
        while (i < jeu.length) {
            if (jeu[i] == "o") {
                labtab.add(new Label("o"));
                labtab.get(labtab.size() - 1).setForeground(Color.red);
                labtab.get(labtab.size() - 1).setFont(new Font("Roboto", Font.BOLD, 15));

            } else {
                if (jeu[i] == "x") {
                    labtab.add(new Label("o"));
                    labtab.get(labtab.size() - 1).setForeground(Color.yellow);
                    labtab.get(labtab.size() - 1).setFont(new Font("Roboto", Font.BOLD, 15));

                } else {
                    if (jeu[i] == "g") {
                        labtab.add(new Label("o"));
                        labtab.get(labtab.size() - 1).setForeground(Color.green);
                        labtab.get(labtab.size() - 1).setFont(new Font("Roboto", Font.BOLD, 15));

                    } else {
                        labtab.add(new Label("-"));
                        labtab.get(labtab.size() - 1).setForeground(Color.gray);
                        labtab.get(labtab.size() - 1).setFont(new Font("Roboto", Font.BOLD, 17));

                    }

                }
            }
            i++;
        }
    }

    /**
     * Méthode qui permet d'afficher le jeu
     * 
     * @param bords L'index du premier bords à placer
     * @param cases l'index de la première case à placer
     */
    public static void rempliraffichagefx(int bords, int cases) {
        int i = 0;

        while (i < 15) {
            if (i % 2 == 0) {
                fenetre.add(bordsTab.get(bords));
                bords++;
            } else {
                fenetre.add(labtab.get(cases));
                cases++;
            }
            i++;
        }
        // fenetre.setVisible(true);
    }

    /**
     * Méthode qui permet de vérifier si le bouton "Jouer" est actionné
     */
    public static void entree() {
        bouton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if ((j1 == null) || (j2 == null)) {
                    boutonActionNoms(evt);

                } else {
                    boutonAction(evt);
                }
            }
        });
    }

    /**
     * Méthode qui permet au joueur de jouer un coup apres avoir appuyer sur "Jouer"
     * 
     * @param evt
     */
    public static void boutonAction(ActionEvent evt) {
        caseJ = Integer.parseInt(saisie.getText());
        saisie.setText(null);
    }

    /**
     * Méthode qui permet de remplir les noms des joueurs
     * 
     * @param evt
     */
    public static void boutonActionNoms(ActionEvent evt) {
        if (j1 == null) {
            j1 = saisie.getText();
            saisie.setText(null);
        } else {
            j2 = saisie.getText();
            saisie.setText(null);
        }

    }

    /**
     * Méthode qui permet de vérifier si le bouton "Quitter" est actionné
     */
    public static void entreeExit() {
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                exitAction(evt);
            }
        });
    }

    /**
     * Méthode qui permet au joueur de jouer un coup apres avoir appuyer sur
     * "Quitter"
     * 
     * @param evt
     */
    public static void exitAction(ActionEvent evt) {
        fenetre.setCursor(new Cursor(3));
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("Fermeture du jeu..");
        System.exit(0);
    }

    /**
     * Méthode qui permet de vérifier si le bouton "Jouer contre le bot" est
     * actionné
     */
    public static void entreeBot() {
        bot.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                botAction(evt);

            }
        });
    }

    /**
     * Méthode qui permet au joueur de jouer un coup apres avoir appuyer sur
     * "Jouer contre le bot"
     * 
     * @param evt
     */
    public static void botAction(ActionEvent evt) {
        j2 = "bot";
    }

}

/*-----------------------------
| .0  | .1  | .2  | .3  | .4  | .5  | 6. |
-----------------------------
| .7  | .8  | .9  | .10 | .11 | .12 | .13 |
-----------------------------
| .14 | .15 | .16 | .17 | .18 | .19 | .20 |
-----------------------------
| .21 | .22 | .23 | .24 | .25 | .26 | .27 |
-----------------------------
| .28 | .29 | .30 | .31 | .32 | .33 | .34 |
-----------------------------
| .35 | .36 | .37 | .38 | .39 | .40 | .41 |
----------------------------- */